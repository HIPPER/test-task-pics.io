# Test task

## Getting Started

These instructions will help you get a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What you need to install the software:

- Docker
- Docker Compose

### Installation

Steps to launch the application:

1. Clone the repository

git clone [URL репозитория]

2. Move to the project directory

cd [название_проекта]

3. Start the application using Docker Compose

docker-compose up --build


# API
All api requests fit such structure:
```
{
    "success": boolean,
    "data": any,
    "message": string
}
```
## Endpoints
|Method|URL|Description|
|--------|----------------------------------------|----------------------------------------|
|`POST`|`/api/auth/login`|[Login](#login-user)|
|`POST`|`/api/auth/register`|[Register](#register-user)|
|`POST`|`/api/event`|[Route an event](#route-event)|

## Authorization
App authorize incoming requests via JWT tokens.
## Requests
### Login User
Request Body Schema
```
{
    "email": string,
    "password": string
}
```

### Register User
Request Body Schema
```
{
    "email": string,
    "password": string
}
```

### Route Event
Request Body Schema
```
{
	payload: { a:1, b:2, c:3 /* any data */ }, 
	possibleDestinations: [
		{
			[string]: boolean
		}
	],
	strategy: 'ALL' | 'ANY' | <<function string>>
	
}
```




