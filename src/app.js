require('dotenv').config()
const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')

const authRoutes = require('./routes/authRoutes')
const eventRoutes = require('./routes/eventRoutes')

const errorHandler = require('./utils/errorHandler')
const logRequestAndResponse = require('./utils/logger')

const keys = require('./config')

const app = express()

async function initDatabase () {
  try {
    await mongoose.connect(keys.MONGO_URI).then(console.log('Connected to MongoDB'))
  } catch (e) {
    console.log('Server Error', e.message)
    process.exit(1)
  }
}

initDatabase()

app.use(
  cors({
    origin: '*',
    credentials: true
  })
)

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(logRequestAndResponse)

app.use('/api/auth/', authRoutes)
app.use('/api/', eventRoutes)

app.use(errorHandler)

app.listen(keys.PORT, () => {
  console.log(`API is listening on port ${keys.PORT}`)
})
