const StrategyAll = require('./StrategyAll')
const StrategyAny = require('./StrategyAny')
const StrategyCustom = require('./StrategyCustom')

const strategies = {
  ALL: new StrategyAll(),
  ANY: new StrategyAny(),
  CUSTOM: new StrategyCustom()
}

const getStrategy = (name) => {
  return strategies[name.toUpperCase()] || strategies.ALL
}

module.exports = { getStrategy }
