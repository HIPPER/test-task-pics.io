class StrategyAll {
  determineDestinations (possibleDestinations) {
    return possibleDestinations.reduce((acc, curr) => {
      Object.keys(curr).forEach(key => {
        if (acc[key] === undefined) {
          acc[key] = true
        }
        acc[key] = acc[key] && curr[key]
      })
      return acc
    }, {})
  }
}

module.exports = StrategyAll
