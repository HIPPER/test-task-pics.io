class StrategyCustom {
  determineDestinations (possibleDestinations) {
    return possibleDestinations.reduce((acc, curr) => {
      Object.keys(curr).forEach(key => {
        acc[key] = acc[key] || curr[key]
      })
      return acc
    }, {})
  }
}

module.exports = StrategyCustom
