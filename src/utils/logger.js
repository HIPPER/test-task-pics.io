const Log = require('../models/Log')

const logRequestAndResponse = (req, res, next) => {
  const oldWrite = res.write
  const oldEnd = res.end

  const chunks = []

  res.write = (...restArgs) => {
    chunks.push(Buffer.from(restArgs[0]))
    oldWrite.apply(res, restArgs)
  }

  res.end = (...restArgs) => {
    if (restArgs[0]) {
      chunks.push(Buffer.from(restArgs[0]))
    }
    const body = Buffer.concat(chunks).toString('utf8')

    oldEnd.apply(res, restArgs)

    const logEntry = new Log({
      request: {
        path: req.path,
        method: req.method,
        body: req.body,
        query: req.query,
        headers: req.headers
      },
      response: {
        status: res.statusCode,
        body // Возможно, нужно будет обработать, чтобы избежать сохранения чувствительных данных
      }
    })

    logEntry.save().catch(err => console.error('Error saving log:', err))
  }

  next()
}

module.exports = logRequestAndResponse
