const authService = require('../services/authService')

// const user = { name: "Example User" };
// const accessToken = jwt.sign(user, process.env.JWT_SECRET, { expiresIn: '1800s' });
// res.json({ accessToken: accessToken });

const register = async (req, res, next) => {
  const { email, password } = req.body
  try {
    await authService.register(email, password)
    res.status(201).json({ message: 'Registered successfully' })
  } catch (error) {
    if (error.code === '23505') {
      res.status(500).json({ error: 'This email already exists' })
    } else {
      error.status = 500
      next(error)
    }
  }
}

const login = async (req, res, next) => {
  const { email, password } = req.body

  try {
    const result = await authService.login(email, password)
    if (!result.status) {
      return res.status(401).json({ error: result.message })
    }

    res.status(200).json({ token: result.token })
  } catch (error) {
    error.status = 500
    next(error)
  }
}

module.exports = {
  register,
  login
}
