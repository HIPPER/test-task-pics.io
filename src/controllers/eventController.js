const eventService = require('../services/eventService')

const handleEvent = async (req, res, next) => {
  const { payload, possibleDestinations, strategy } = req.body

  try {
    const result = await eventService.routeEvent(payload, possibleDestinations, strategy)
    res.json(result)
  } catch (error) {
    error.status = 500
    next(error)
  }
}

module.exports = { handleEvent }
