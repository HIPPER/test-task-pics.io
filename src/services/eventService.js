const { getStrategy } = require('../strategies')

const destinationsConfig = [
  { name: 'destination1', transport: 'http.post' },
  { name: 'destination2', transport: 'http.put' },
  { name: 'destination3', transport: 'http.get' },
  { name: 'destination4', transport: 'console.log' }
]

const executeStrategy = (strategyName, possibleDestinations) => {
  const strategy = getStrategy(strategyName)
  return strategy.determineDestinations(possibleDestinations)
}

const routeEvent = async (payload, possibleDestinations, strategyName = 'ALL') => {
  const destinationsResults = executeStrategy(strategyName, possibleDestinations)

  const response = destinationsConfig.reduce((acc, { name }) => {
    acc[name] = !!destinationsResults[name]
    return acc
  }, {})

  Object.keys(destinationsResults).forEach(key => {
    if (!Object.prototype.hasOwnProperty.call(response, key)) {
      console.log(`UnknownDestinationError: ${key}`)
      response[key] = false
    }
  })

  return response
}

module.exports = { routeEvent }
