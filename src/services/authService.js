const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const keys = require('../config')

const User = require('../models/User')

const register = async (email, password) => {
  const hashedPassword = await bcrypt.hash(password, 10)
  const user = await User({ email, password: hashedPassword })
  await user.save()
}

const login = async (email, password) => {
  const user = await User.findOne({ email })

  const result = {
    status: true,
    message: null
  }

  if (!user) {
    result.status = false
    result.message = 'Invalid email'
    return result
  }

  const isMatch = await bcrypt.compare(password, user.password)
  if (!isMatch) {
    result.status = false
    result.message = 'Invalid password'
    return result
  }

  const accessToken = jwt.sign(user.toJSON(), keys.JWT_SECRET, { expiresIn: '1800s' })

  result.token = accessToken
  return result
}

module.exports = {
  register,
  login
}
