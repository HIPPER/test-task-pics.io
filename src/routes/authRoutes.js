const express = require('express')
const { body } = require('express-validator')

const validation = require('../middleware/validation')

const authController = require('../controllers/authController')

const router = express.Router()

router.post('/register', validation, [body('email').isEmail(), body('password').isLength({ min: 8 })], authController.register)

router.post('/login', validation, [body('email').isEmail(), body('password').isLength({ min: 8 })], authController.login)

module.exports = router
