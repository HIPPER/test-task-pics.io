const express = require('express')
const authenticateToken = require('../middleware/auth')
const { handleEvent } = require('../controllers/eventController')

const router = express.Router()

router.post('/event', authenticateToken, handleEvent)

module.exports = router
