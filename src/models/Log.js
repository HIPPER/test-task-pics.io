const mongoose = require('mongoose')

const logSchema = new mongoose.Schema({
  request: {
    path: String,
    method: String,
    body: mongoose.Schema.Types.Mixed,
    query: mongoose.Schema.Types.Mixed,
    headers: mongoose.Schema.Types.Mixed
  },
  response: {
    status: Number,
    body: mongoose.Schema.Types.Mixed
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('Log', logSchema)
